// Рівносторонній трикутник
const line = 8;
let space = 7,
    star = 1;
for (let h = 0; h < line; h++) {
    for (let wsp = 0; wsp < space; wsp++) {
        document.write("&nbsp\n");
    }
    for (wst = 0; wst < star; wst++) {
        document.write("*");
    }
    space -= 1;
    star += 2;
    document.write("<br>");
}
