function Human(name, age, sex) {
  this.name = name;
  this.age = age;
  this.sex = sex;
  Human.prototype.isAdult = function() {
    (this.age >= 18) ? document.write(`I am adult <br>`): document.write(`I am child <br>`) 
  };
  Human.prototype.concat = function() {
    document.write(`My name is ${this.name}. I am ${this.age} years old. I am a ${this.sex}. <br>`); 
  }
}

let john = new Human ('John', 33, 'M');
let ken = new Human ('Ken', 42, 'M');
let mary = new Human ('Mary', 14, 'W');
let nick = new Human ('Nick', 31, 'M');
let den = new Human ('Den', 16, 'M');
let kate = new Human ('Kate', 55, 'W');
const arrayHuman = [john, ken, mary, nick, den, kate];
console.log (arrayHuman);
john.concat();
den.concat();
mary.isAdult();
kate.isAdult();