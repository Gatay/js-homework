const circleButton = document.getElementById("circleButton");
circleButton.onclick = createInput;
// функція створює input and buuton
function createInput() {
  let label = document.createElement("label");
  label.setAttribute("for", "diameter");
  label.innerHTML = "Введіть число";
  document.body.append(label);
  //create input element
  let input = document.createElement("input");
  input.setAttribute("class", "diameter-input");
  input.setAttribute("type", "number");
  input.setAttribute("value", "cm");
  document.body.append(input);
  // create button element
  let drowButton = document.createElement("button");
  drowButton.setAttribute("class", "drow-button");
  document.body.append(drowButton);
  let textButton = document.createTextNode("Намалювати");
  drowButton.append(textButton);
  // delete circleButton element
  circleButton.remove();

  drowButton.onclick = function () {
    // delete all elements
    input.remove();
    label.remove();
    drowButton.remove();
    // create circles
    let d = input.value;
    document.body.insertAdjacentHTML('beforeend', '<div id="cont">' + '<div></div>'.repeat(100) + '</div>');
    document.querySelectorAll('#cont>div').forEach(el => {
      el.style.backgroundColor = '#' + Math.random().toString(16).slice(2, 8);
      el.style.width = `${d}cm`;
      el.style.height = `${d}cm`;
      el.onclick = e => el.style.display = 'none';

    });

  }
}